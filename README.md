#This application was created by Andrei Osipov as a test exercise for a JetBrains company.
#
#
#
#The purpose of this application is to expose on localhost:8080 service, which allows to convert long URLs into short.
#Selected provider to short URLs is http://gg.gg/
#Additional libs to short URL were used from this package: github.com/subosito/shorturl
