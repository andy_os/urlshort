package main

import (
	"html/template"
	"log"
	"net/http"
	"github.com/subosito/shorturl"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

type link struct {
	LongUrl  string
	SmallUrl string
}

func main() {
	http.HandleFunc("/", foo)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {

	f := req.FormValue("first")
	provider := "gggg"
	SmallUrl, _ := shorturl.Shorten(f, provider)
	err := tpl.ExecuteTemplate(w, "index.gohtml", link{f,SmallUrl})
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Fatalln(err)
	}
}
