#used multi stage build since the initial size of a golang image is too big for a test application
#make sure than docker engine is greater than 1.17 otherwise multistaging feature won't work, you can check it by 'docker version'
FROM golang:latest as builder
LABEL maintainer="Andrei Osipov"
WORKDIR /app
RUN go get github.com/subosito/shorturl 
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .


FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /app/main .
COPY . .
EXPOSE 8080
CMD ["./main"] 
